﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleCodeGenApp.Model.Compiler;

namespace ExampleCodeGenApp.Model {
    public class ExpressionCall : IStatement {
        public string FunctionName { get; set; }
        public List<IStatement> Parameters { get; } = new List<IStatement>();

        public string Compile(CompilerContext context) {
            var t1 = new String(' ', 2 * context.VariablesScopesStack.Count);
            context.EnterNewScope(context.FindFreeVariableName());
            var t2 = new String(' ', 2 * context.VariablesScopesStack.Count);
            var arguments = Parameters.Count > 0 ? $"\n{t2}{String.Join($",\n{t2}", Parameters.Select(p => p?.Compile(context)))}\n{t1}" : "";
            context.LeaveScope();
            return $"{FunctionName}[{arguments}]";
        }
    }
}
