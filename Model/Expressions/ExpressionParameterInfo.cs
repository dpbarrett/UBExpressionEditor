﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleCodeGenApp.Model.Expressions {
    public class ExpressionParameterInfo {
        public string Name { get; }
        public Type Type { get; }
        public string Description { get; }
        public string FriendlyType {
            get {
                return Type.ToString().ToLower().Split('.').Last().Split('+').Last().Replace("double", "number");
            }
        }

        public ExpressionParameterInfo(string name, Type type, string description) {
            Name = name;
            Type = type;
            Description = description;
        }
    }
}
