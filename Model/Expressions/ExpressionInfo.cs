﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExampleCodeGenApp.Model.Expressions {
    public class ExpressionInfo {
        public string MethodName { get; }
        public string ToolName { get; }
        public List<ExpressionParameterInfo> Parameters { get; }
        public string Summary { get; }
        public Dictionary<string, string> Examples { get; }
        public ExpressionReturnInfo ReturnInfo { get; }

        public string MethodSignature {
            get {
                if (!string.IsNullOrEmpty(ToolName) && Parameters != null)
                    return $"{MethodName}[{string.Join(", ", Parameters.Select(p => $"{ p.FriendlyType} {p.Name}").ToArray())}]";
                else
                    return MethodName;
            }
        }

        public string ReturnSummary {
            get {
                if (ReturnInfo != null)
                    return $"Returns - ({ReturnInfo.FriendlyType}) {ReturnInfo.Description}";
                else
                    return "";
            }
        }

        public ExpressionInfo(string methodName, string toolName, string summary, List<ExpressionParameterInfo> parameters, Dictionary<string, string> examples, ExpressionReturnInfo returnInfo) {
            MethodName = methodName;
            ToolName = toolName;
            Parameters = parameters;
            Summary = summary;
            Examples = examples;
            ReturnInfo = returnInfo;
        }
    }
}
