﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExampleCodeGenApp.Model.Expressions;
using ExampleCodeGenApp.ViewModels;
using NodeNetwork.Views;
using ReactiveUI;

namespace ExampleCodeGenApp.Views {
    public partial class CodeGenNodeView : IViewFor<CodeGenNodeViewModel> {
        #region ViewModel
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(nameof(ViewModel),
            typeof(CodeGenNodeViewModel), typeof(CodeGenNodeView), new PropertyMetadata(null));

        public CodeGenNodeViewModel ViewModel {
            get => (CodeGenNodeViewModel)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel {
            get => ViewModel;
            set => ViewModel = (CodeGenNodeViewModel)value;
        }
        #endregion

        public static readonly DependencyProperty ExpressionInfoProperty =
        DependencyProperty.RegisterAttached("ExpressionInfo", typeof(ExpressionInfo), typeof(CodeGenNodeView), new PropertyMetadata(null));

        public ExpressionInfo ExpressionInfo {
            get { return (ExpressionInfo)GetValue(ExpressionInfoProperty); }
            set { SetValue(ExpressionInfoProperty, value); }
        }

        public CodeGenNodeView() {
            InitializeComponent();

            this.WhenActivated(d => {
                ExpressionInfo = (this.ViewModel as CodeGenNodeViewModel).ExpressionInfo;
                NodeView.ViewModel = this.ViewModel;
                Disposable.Create(() => NodeView.ViewModel = null).DisposeWith(d);

                this.OneWayBind(ViewModel, vm => vm.NodeType, v => v.NodeView.Background, ConvertNodeTypeToBrush).DisposeWith(d);
            });
        }

        private Brush ConvertNodeTypeToBrush(NodeType type) {
            switch (type) {
                case NodeType.ResultNode: return new SolidColorBrush(Color.FromRgb(83, 32, 32));
                case NodeType.Function: return new SolidColorBrush(Color.FromRgb(32, 55, 83));
                case NodeType.Number: return new SolidColorBrush(Color.FromRgb(26, 50, 20));
                case NodeType.Text: return new SolidColorBrush(Color.FromRgb(0x42, 0xd4, 0xf4));
                default: throw new Exception("Unsupported node type");
            }
        }
    }
}
