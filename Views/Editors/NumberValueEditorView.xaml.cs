﻿using System.Windows;
using ExampleCodeGenApp.ViewModels;
using ExampleCodeGenApp.ViewModels.Editors;
using ReactiveUI;

namespace ExampleCodeGenApp.Views.Editors
{
    public partial class NumberValueEditorView : IViewFor<NumberValueEditorViewModel>
    {
        #region ViewModel
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register(nameof(ViewModel),
            typeof(NumberValueEditorViewModel), typeof(NumberValueEditorView), new PropertyMetadata(null));

        public NumberValueEditorViewModel ViewModel
        {
            get => (NumberValueEditorViewModel)GetValue(ViewModelProperty);
            set => SetValue(ViewModelProperty, value);
        }

        object IViewFor.ViewModel
        {
            get => ViewModel;
            set => ViewModel = (NumberValueEditorViewModel)value;
        }
        #endregion

        public NumberValueEditorView()
        {
            InitializeComponent();

            this.WhenActivated(d => d(
                this.Bind(ViewModel, vm => vm.Value, v => v.UpDown.Text)
            ));
        }
    }
}
