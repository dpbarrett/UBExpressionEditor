## Expression Editor
An experimental visual expression editor for creating [UtilityBelt Expressions](https://utilitybelt.gitlab.io/docs/expressions/) graphically.

### Setup
1. Download from releases
2. Make sure you have [dotnet 4.7.2](https://dotnet.microsoft.com/download/dotnet-framework/net472) installed
3. Open up ExpressionEditor.exe

### Usage
- Drag nodes from the left list view over to the canvas to add them.
- Right click drag to create a "cut" line to destroy existing connections
- Drag node input/outputs to other nodes to form an expression
- Shift click drag to select multiple nodes
- Click drag anywhere on the canvas to move around
- Mousewheel on canvas changes zoom level

