﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Antlr4.Runtime;
using ExampleCodeGenApp.Model.Compiler;
using ExampleCodeGenApp.Model.Compiler.Error;
using ReactiveUI;
using UtilityBelt;
using UtilityBelt.Lib.Expressions;

namespace ExampleCodeGenApp.ViewModels {
    public class CodePreviewViewModel : ReactiveObject {
        #region Code
        public IStatement Code {
            get => _code;
            set => this.RaiseAndSetIfChanged(ref _code, value);
        }
        private IStatement _code;
        #endregion

        #region CompilerError
        public string CompilerError {
            get => _compilerError;
            set => this.RaiseAndSetIfChanged(ref _compilerError, value);
        }
        private string _compilerError;
        #endregion

        #region CompiledCode
        private readonly ObservableAsPropertyHelper<string> _compiledCode;
        public string CompiledCode => _compiledCode.Value;
        #endregion

        public string ExpressionTextBox { get; set; } = "`test`";

        public ReactiveCommand<Unit, Unit> CopyToClipboard { get; }
        public ReactiveCommand<Unit, Unit> Import { get; }

        public CodePreviewViewModel() {
            CopyToClipboard = ReactiveCommand.Create(() => {
                var data = "";
                if (!string.IsNullOrEmpty(CompiledCode)) {
                    data = string.Join("", CompiledCode.Replace("\r", "").Split('\n').Select(c => c.Trim()));
                    if (!string.IsNullOrEmpty(data))
                        Clipboard.SetText(data);
                }
            });

            Import = ReactiveCommand.Create(() => {
                TryParseExpression(ExpressionTextBox);
            });

            this.WhenAnyValue(vm => vm.Code).Where(c => c != null)
                .Select(c => {
                    CompilerError = "";
                    CompilerContext ctx = new CompilerContext();

                    try {
                        return c.Compile(ctx);
                    }
                    catch (CompilerException e) {
                        string trace = string.Join("\n", ctx.VariablesScopesStack.Select(s => s.Identifier));
                        CompilerError = e.Message + "\nProblem is near:\n" + trace;
                        return "";
                    }
                })
                .ToProperty(this, vm => vm.CompiledCode, out _compiledCode);
        }

        class ExpressionErrorListener : DefaultErrorStrategy {
            public override void ReportError(Parser recognizer, RecognitionException e) {
                throw new Exception($"Expression Error: {e.Message} @ char position {e.OffendingToken.Column}");
            }
        }

        private void TryParseExpression(string expressionText) {
            try {
                AntlrInputStream inputStream = new AntlrInputStream(expressionText);
                MetaExpressionsLexer spreadsheetLexer = new MetaExpressionsLexer(inputStream);
                CommonTokenStream commonTokenStream = new CommonTokenStream(spreadsheetLexer);
                MetaExpressionsParser expressionParser = new MetaExpressionsParser(commonTokenStream);
                expressionParser.ErrorHandler = new ExpressionErrorListener();
                MetaExpressionsParser.ParseContext parseContext = expressionParser.parse();
                Lib.ExpressionVisitor visitor = new Lib.ExpressionVisitor();
            }
            catch (Exception ex) {
                CompilerError = ex.Message;
            }
        }
    }
}
