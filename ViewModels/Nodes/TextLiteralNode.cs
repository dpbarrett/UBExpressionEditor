﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicData;
using ExampleCodeGenApp.Model;
using ExampleCodeGenApp.Model.Compiler;
using ExampleCodeGenApp.Model.Expressions;
using ExampleCodeGenApp.ViewModels.Editors;
using ExampleCodeGenApp.Views;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels.Nodes {
    public class TextLiteralNode : CodeGenNodeViewModel {
        static TextLiteralNode() {
            Splat.Locator.CurrentMutable.Register(() => new CodeGenNodeView(), typeof(IViewFor<TextLiteralNode>));
        }

        public StringValueEditorViewModel ValueEditor { get; } = new StringValueEditorViewModel();

        public TextLiteralNode() : base(NodeType.Number) {
            this.Name = "Text";
            this.Category = "Literals";

            this.ExpressionInfo = new Model.Expressions.ExpressionInfo("Text", null, "Represents a string of text", null, null, new ExpressionReturnInfo(typeof(string), "A string of text"));

            Output = new CodeGenOutputViewModel<IStatement>(PortType.String, ExpressionInfo.ReturnInfo) {
                Name = "Value",
                Editor = ValueEditor,
                Value = ValueEditor.ValueChanged.Select(v => new StringLiteral { Value = v }),
                PortPosition = PortPosition.Right
            };

            this.Outputs.Add(Output);
        }
    }
}
