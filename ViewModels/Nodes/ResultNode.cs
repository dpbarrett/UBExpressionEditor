﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicData;
using ExampleCodeGenApp.Model.Compiler;
using ExampleCodeGenApp.Views;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels.Nodes {
    public class ResultNode : CodeGenNodeViewModel {
        static ResultNode() {
            Splat.Locator.CurrentMutable.Register(() => new CodeGenNodeView(), typeof(IViewFor<ResultNode>));
        }

        public ValueListNodeInputViewModel<IStatement> ResultInputs { get; }

        public ResultNode() : base(NodeType.ResultNode) {
            this.Name = "Result";

            this.ExpressionInfo = new Model.Expressions.ExpressionInfo("Result", null, "This is the 'end' of your expression, the final output.", null, null, null);

            ResultInputs = new CodeGenListInputViewModel<IStatement>(PortType.Execution, null) {
                Name = "Result",
                MaxConnections = 1,
                PortPosition = PortPosition.Left,
                ConnectionValidator = (pending) => {
                    // accepts all connections
                    return new NodeNetwork.ConnectionValidationResult(true, null);
                }
            };

            this.Inputs.Add(ResultInputs);
        }
    }
}
