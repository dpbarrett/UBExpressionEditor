﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExampleCodeGenApp.Model.Expressions;
using NodeNetwork.Toolkit.ValueNode;
using NodeNetwork.ViewModels;
using NodeNetwork.Views;
using ReactiveUI;

namespace ExampleCodeGenApp.ViewModels {
    public class CodeGenListInputViewModel<T> : ValueListNodeInputViewModel<T> {
        static CodeGenListInputViewModel() {
            Splat.Locator.CurrentMutable.Register(() => new NodeInputView(), typeof(IViewFor<CodeGenListInputViewModel<T>>));
        }

        public ExpressionParameterInfo ParameterInfo { get; }

        public CodeGenListInputViewModel(PortType type, ExpressionParameterInfo parameterInfo) {
            this.ParameterInfo = parameterInfo;
            this.Port = new CodeGenPortViewModel { PortType = type };

            if (type == PortType.Execution) {
                this.PortPosition = PortPosition.Right;
            }
        }
    }
}
